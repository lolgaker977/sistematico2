package ni.edu.uni.archivos.pojo;

public enum EstadoActivoFijo {
    ACTIVO(1), INACTIVO(2), ASIGNADO(3), MANTENIMIENTO(4), MALESTADO(5);

    int estado;

    private EstadoActivoFijo(int opc){
        this.estado = opc;
    }

    public int getEstado(){
        return estado;
    }
}
